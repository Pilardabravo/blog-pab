'use strict';

const express = require('express');
const path = require('path');

//Constantes
const PORT = 8081;

//App
const app = express();
//aqui ya escucharia pero no haŕia nada
app.use(express.static(__dirname));
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname+'/index.html'));
//  res.send("Bienvenido terricola\n"); paso1
});
//cuando llegue una petición a la raiz, va a ejecutar una funcion
// lo normal es devolver un html
app.get('/detallePosts/:id', function(req, res) {
  console.log(req);
  res.sendFile(path.join(__dirname+'/detallePosts.html'));
});

app.get('/admin', function(req, res) {
  console.log(req);
  res.sendFile(path.join(__dirname+'/histograms.html'));
});

app.get('/homeh', function(req, res) {
  console.log(req);
  res.sendFile(path.join(__dirname+'/scr/my-homeh.html'));
});

app.listen(PORT);
//escucha el puerto
console.log('Express funcionando en el puerto' + PORT);
//grabamos linea en el log, con el puero dinamico
